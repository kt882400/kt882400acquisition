from csv import writer
from serial import Serial, SerialException, to_bytes
from time import time

class Kt88DataReader:
    def __init__(self, port, output_file_path='c:\\out', baud_rate=921600, byte_size=8, stop_bits=1, parity='N'): #timeout
        self.__port = port
        self.__baud_rate = baud_rate
        self.__byte_size = byte_size
        self.__stop_bits = stop_bits
        self.__parity = parity

        self.__READ_BUFFER_SIZE = 54
        self.__terminate_data_acquisition = True
        self.time_read_start = 0

        self.__output_raw = []
        self.__output_file_path = output_file_path
        self.__ELECTRODE_NUMBER = 19



    def start_aquisition(self):
        self.__terminate_data_acquisition = False
        self.__read_buffer_cache = [0] * self.__READ_BUFFER_SIZE
        self.__synchronization_index = -1;
        f = open(self.__output_file_path, "a")

        frame_counter = 0

        while (self.__terminate_data_acquisition == False):
            try:
                with Serial(port=self.__port,
                            baudrate=self.__baud_rate,
                            bytesize=self.__byte_size,
                            stopbits=self.__stop_bits,
                            parity=self.__parity,
                            timeout=5) as kt88_serial:
                    kt88_serial.write(to_bytes([0x08]))
                    bytes_read = kt88_serial.read(self.__READ_BUFFER_SIZE)

                    if len(bytes_read) != self.__READ_BUFFER_SIZE:
                        print('Serial port read error.'+ (len(bytes_read) + 'bytes acquired, read buffer size is ' + self.__READ_BUFFER_SIZE))

                    kt88_serial.write(to_bytes([0x83]))
                    kt88_serial.write(to_bytes([0x88]))

                    self.time_read_start = time()

                    while (self.__terminate_data_acquisition == False):
                        try:
                            read_buffer = kt88_serial.read(self.__READ_BUFFER_SIZE)

                            if len(read_buffer) != self.__READ_BUFFER_SIZE:
                                print('Serial port read error.'+ (len(bytes_read) + 'bytes acquired, read buffer size is ' + self.__READ_BUFFER_SIZE))

                            #synchronize frame
                            frame_synchronization_index_tmp = -1

                            for i in range(self.__READ_BUFFER_SIZE - 1):
                                if read_buffer[i] == 255 and read_buffer[i + 1] == 255:
                                    frame_synchronization_index_tmp = i
                                    break

                            if (frame_synchronization_index_tmp == -1) and (read_buffer[0] == 255 and read_buffer[self.__READ_BUFFER_SIZE - 1] == 255):
                                self.__shift_buffer_elements_to_the_right(read_buffer)
                                self.__read_buffer_cache = read_buffer
                                self.__synchronization_index = self.__READ_BUFFER_SIZE
                                frame_synchronization_index_tmp = self.__synchronization_index
                            elif frame_synchronization_index_tmp == -1 and not (read_buffer[0] == 255 and read_buffer[self.__READ_BUFFER_SIZE - 1] == 255):
                                #wrong data in the frame
                                continue

                            if frame_synchronization_index_tmp != self.__synchronization_index and frame_counter != 0:
                                self.__synchronization_index = frame_synchronization_index_tmp

                                for i in range(self.__READ_BUFFER_SIZE - self.__synchronization_index):
                                    self.__read_buffer_cache[i] = read_buffer[i + self.__synchronization_index]

                                for i in range(self.__READ_BUFFER_SIZE - self.__synchronization_index, self.__READ_BUFFER_SIZE):
                                    self.__read_buffer_cache[i] = 0
                                continue
                            elif frame_synchronization_index_tmp != self.__synchronization_index and frame_counter == 0:
                                self.__synchronization_index = frame_synchronization_index_tmp

                            for i in range(self.__synchronization_index):
                                self.__read_buffer_cache[self.__READ_BUFFER_SIZE - self.__synchronization_index + i] = read_buffer[i]

                            for i in range(self.__READ_BUFFER_SIZE - self.__synchronization_index):
                                read_buffer[i] = read_buffer[i + self.__synchronization_index]

                            for i in range(self.__READ_BUFFER_SIZE - self.__synchronization_index, self.__READ_BUFFER_SIZE):
                                read_buffer[i] = 0

                            packet = [0] * (self.__ELECTRODE_NUMBER + 1)

                            for i in range(len(packet)):
                                packet[i] = ((self.__read_buffer_cache[i * 2] & 0xFF) * 16) \
                                            + (self.__read_buffer_cache[1 + i * 2] & 0x0F) \
                                            - 2048


                            if frame_counter != 0:
                                packet.append(int(time() - self.__data_provider.time_read_start))
                                self.__output_raw.append(self.__read_buffer_cache)
                                f.write(self.__output_raw);
                                f.write('\n')
                                self.__output_raw.clear()

                            self.__read_buffer_cache, read_buffer = list(read_buffer), self.__read_buffer_cache
                            frame_counter += 1
                        except Exception as e:
                            print(str(e))
            except SerialException as e:
                print(str(e))
            f.close()

    def __shift_buffer_elements_to_the_right(self, read_buffer):
        tmp1, tmp2 = 255, 255
        for i in range(self.__READ_BUFFER_SIZE):
            tmp1 = read_buffer[i]
            read_buffer[i] = tmp2
            tmp2 = tmp1

if __name__ == "__main__":
   kt_reader = Kt88DataReader(port='COM3', output_file_path='c:\\out' )
   kt_reader.start_aquisition()
